import { mount } from 'avoriaz'
import List from '@/components/List'

describe('List.vue', () => {
  // previous tests ..
  it('displays items from the list', () => {
    const ListComponent = mount(List)
    expect(ListComponent.text()).to.contain('play games')
  })

  it('adds new item to list on click with avoriaz', () => {
    // build component
    const ListComponent = mount(List)

    // set input value
    const action = 'brush my teeth'
    ListComponent.setData({
      newItem: action
    })

    // simulate click event
    const button = ListComponent.find('button')[0]
    button.trigger('click')

    // assert list contains new item
    expect(ListComponent.text()).to.contain(action)
    expect(ListComponent.data().listItems).to.contain(action)
  })
})
